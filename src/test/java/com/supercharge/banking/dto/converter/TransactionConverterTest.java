package com.supercharge.banking.dto.converter;

import com.supercharge.banking.dto.Transaction;
import com.supercharge.banking.dto.TransactionEvent;
import org.junit.Test;

import java.time.LocalDateTime;

import static org.assertj.core.api.Assertions.assertThat;

public class TransactionConverterTest {

    private static final int AMOUNT = 5;

    private TransactionConverter underTest = new TransactionConverter();

    @Test
    public void convertShouldConvertToTransaction() {
        //GIVEN
        int newBalance = 0;
        LocalDateTime now = LocalDateTime.now();
        TransactionEvent event = new TransactionEvent(now, AMOUNT);
        Transaction expected = Transaction.builder()
                                          .date(now)
                                          .amount(AMOUNT)
                                          .balance(newBalance)
                                          .build();

        //WHEN
        Transaction actual = underTest.convert(event, newBalance);

        //THEN
        assertThat(actual).isEqualTo(expected);
    }

}