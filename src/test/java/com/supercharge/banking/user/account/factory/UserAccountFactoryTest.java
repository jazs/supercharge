package com.supercharge.banking.user.account.factory;

import com.supercharge.banking.dto.Transaction;
import com.supercharge.banking.dto.converter.TransactionConverter;
import com.supercharge.banking.user.account.UserAccount;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.time.LocalDateTime;
import java.util.LinkedList;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.BDDMockito.given;

@RunWith(MockitoJUnitRunner.class)
public class UserAccountFactoryTest {

    @Mock
    private TransactionConverter converter;

    @Mock
    private InitialTransactionListFactory initialTransactionListFactory;

    private UserAccountFactory underTest;

    @Before
    public void setUp() throws Exception {
        LinkedList<Transaction> transactions = getInitialTransactions();
        given(initialTransactionListFactory.create()).willReturn(transactions);
        underTest = new UserAccountFactory(converter, initialTransactionListFactory);
    }

    @Test
    public void createShouldCreateNewUserAccount() throws Exception {
        //GIVEN
        UserAccount firstAccount = underTest.create();

        //WHEN
        UserAccount secondAccount = underTest.create();

        //THEN
        assertThat(secondAccount).isEqualTo(firstAccount);
        assertThat(secondAccount).isNotSameAs(firstAccount);
    }


    private LinkedList<Transaction> getInitialTransactions() {
        LinkedList<Transaction> transactions = new LinkedList<>();
        Transaction initialTransaction = Transaction.builder()
                                                    .date(LocalDateTime.now())
                                                    .amount(0)
                                                    .balance(0)
                                                    .build();
        transactions.add(initialTransaction);

        return transactions;
    }
}