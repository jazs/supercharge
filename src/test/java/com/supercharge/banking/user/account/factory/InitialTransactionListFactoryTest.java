package com.supercharge.banking.user.account.factory;

import com.supercharge.banking.dto.Transaction;
import org.junit.Before;
import org.junit.Test;

import java.time.Clock;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.LinkedList;

import static org.assertj.core.api.Assertions.assertThat;

public class InitialTransactionListFactoryTest {

    private Clock clock = Clock.fixed(Instant.parse("2010-10-10T10:10:10.00Z"), ZoneOffset.UTC);

    private InitialTransactionListFactory underTest;

    @Before
    public void setUp() {
        underTest = new InitialTransactionListFactory(clock);
    }

    @Test
    public void createShouldProvideInitialTransactionList() throws Exception {
        //GIVEN
        LinkedList<Transaction> expected = new LinkedList<>();
        Transaction initialTransaction = Transaction.builder()
                                                    .date(LocalDateTime.now(clock))
                                                    .amount(0)
                                                    .balance(0)
                                                    .build();

        expected.add(initialTransaction);

        //WHEN
        LinkedList<Transaction> actual = underTest.create();

        //THEN
        assertThat(actual).isEqualTo(expected);
    }
}