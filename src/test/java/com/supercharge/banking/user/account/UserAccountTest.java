package com.supercharge.banking.user.account;


import com.supercharge.banking.dto.Transaction;
import com.supercharge.banking.dto.TransactionEvent;
import com.supercharge.banking.dto.converter.TransactionConverter;
import com.supercharge.banking.user.account.factory.InitialTransactionListFactory;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.time.LocalDateTime;
import java.util.LinkedList;
import java.util.List;

import static java.util.Arrays.asList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;

@RunWith(MockitoJUnitRunner.class)
public class UserAccountTest {

    private static final String INVALID_BALANCE = "Invalid transaction, account balance can not be smaller than 0";

    private static final int NEGATIVE_AMOUNT = -5;

    private static final int POSITIVE_AMOUNT = 5;

    private LocalDateTime now = LocalDateTime.now();

    private TransactionEvent negativeAmountEvent = new TransactionEvent(now, NEGATIVE_AMOUNT);

    private TransactionEvent positiveAmountEvent = new TransactionEvent(now, POSITIVE_AMOUNT);

    private LinkedList<Transaction> transactions = getInitialTransactions(now);

    @Mock
    private TransactionConverter converter;

    @Mock
    private InitialTransactionListFactory factory;

    private UserAccount underTest;

    @Before
    public void setUp() {
        given(factory.create()).willReturn(transactions);
        underTest = new UserAccount(converter, factory);
    }

    @Test
    public void performTransactionShouldThrowExceptionWhenNewBalanceSmallerThanZero() {
        //GIVEN

        //WHEN
        Throwable actual = catchThrowable(() -> underTest.performTransaction(negativeAmountEvent));

        //THEN
        assertThat(actual).isInstanceOf(IllegalStateException.class);
        assertThat(actual.getMessage()).isEqualTo(INVALID_BALANCE);
        verify(factory).create();
        verifyNoMoreInteractions(converter, factory);
    }

    @Test
    public void performTransactionShouldCallConverter() {
        //GIVEN
        Transaction initialTransaction = createTransaction(now, 0, 0);
        Transaction transaction1 = createTransaction(now, POSITIVE_AMOUNT, 5);
        Transaction transaction2 = createTransaction(now, POSITIVE_AMOUNT, 10);
        List<Transaction> transactions = asList(initialTransaction, transaction1, transaction2);

        given(converter.convert(positiveAmountEvent, 5)).willReturn(transaction1);
        given(converter.convert(positiveAmountEvent, 10)).willReturn(transaction2);

        //WHEN
        underTest.performTransaction(positiveAmountEvent);
        underTest.performTransaction(positiveAmountEvent);

        //THEN
        verify(factory).create();
        verify(converter).convert(positiveAmountEvent, 5);
        verify(converter).convert(positiveAmountEvent, 10);
        verifyNoMoreInteractions(converter, factory);
    }

    @Test
    public void getTransactionViewShouldReturnAViewOfTransactions() throws Exception {
        //GIVEN
        Transaction initialTransaction = createTransaction(now, 0, 0);
        Transaction transaction1 = createTransaction(now, POSITIVE_AMOUNT, 5);
        Transaction transaction2 = createTransaction(now, POSITIVE_AMOUNT, 10);


        List<Transaction> expected = asList(createTransaction(now, 0, 10),
                                            createTransaction(now, POSITIVE_AMOUNT, 10),
                                            createTransaction(now, POSITIVE_AMOUNT, 10));

        given(converter.convert(positiveAmountEvent, 5)).willReturn(transaction1);
        given(converter.convert(positiveAmountEvent, 10)).willReturn(transaction2);
        underTest.performTransaction(positiveAmountEvent);
        underTest.performTransaction(positiveAmountEvent);

        //WHEN
        List<Transaction> actual = underTest.getTransactionsView();

        //THEN
        assertThat(actual).isEqualTo(expected);
    }

    private LinkedList<Transaction> getInitialTransactions(LocalDateTime localDateTime) {
        LinkedList<Transaction> transactions = new LinkedList<>();
        Transaction initialTransaction = createTransaction(localDateTime, 0, 0);

        transactions.add(initialTransaction);

        return transactions;
    }

    private Transaction createTransaction(LocalDateTime localDateTime, int amount, int balance) {
        return Transaction.builder()
                          .date(localDateTime)
                          .amount(amount)
                          .balance(balance)
                          .build();
    }
}