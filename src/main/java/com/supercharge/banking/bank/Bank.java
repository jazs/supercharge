package com.supercharge.banking.bank;

import com.supercharge.banking.bank.filter.TransactionHistoryFilter;
import com.supercharge.banking.dto.Transaction;
import com.supercharge.banking.dto.TransactionEvent;
import com.supercharge.banking.user.User;
import com.supercharge.banking.user.account.UserAccount;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;

@Component
@RequiredArgsConstructor
public class Bank {
    private Map<String, User> userLookup = new HashMap<>();

    public void addUser(User user) {
        userLookup.put(user.getName(), user);
    }

    public void deposit(LocalDateTime localDateTime, String username, int amount) {
        handleInvalidAmount(amount);
        TransactionEvent depositEvent = new TransactionEvent(localDateTime, amount);
        userLookup.get(username)
                  .getUserAccount()
                  .performTransaction(depositEvent);

    }

    public void withdrawal(LocalDateTime localDateTime, String username, int amount) {
        handleInvalidAmount(amount);
        TransactionEvent withdrawalEvent = new TransactionEvent(localDateTime, -amount);
        userLookup.get(username)
                  .getUserAccount()
                  .performTransaction(withdrawalEvent);
    }

    public void transfer(LocalDateTime localDateTime, String userNameFrom, String userNameTo, int amount) {
        handleInvalidAmount(amount);
        withdrawal(localDateTime, userNameFrom, amount);
        deposit(localDateTime, userNameTo, amount);
    }

    public void printHistory(String userName, List<TransactionHistoryFilter> filters) {
        UserAccount userAccount = userLookup.get(userName)
                                            .getUserAccount();
        Stream<Transaction> transactionStream = userAccount.getTransactionsView()
                                                           .stream();
        for (TransactionHistoryFilter filter : filters) {
            transactionStream = filter.filter(transactionStream);
        }
        transactionStream.forEach(System.out::println);
    }

    private void handleInvalidAmount(int amount) {
        if (amount <= 0) {
            throw new IllegalArgumentException("Amount should be positive!");
        }
    }

}
