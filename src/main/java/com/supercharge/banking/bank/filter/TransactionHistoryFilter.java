package com.supercharge.banking.bank.filter;

import com.supercharge.banking.dto.Transaction;

import java.util.stream.Stream;

public interface TransactionHistoryFilter {

    Stream<Transaction> filter (Stream<Transaction> transactions);

}
