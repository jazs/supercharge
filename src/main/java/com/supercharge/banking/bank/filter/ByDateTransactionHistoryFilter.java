package com.supercharge.banking.bank.filter;

import com.supercharge.banking.dto.Transaction;
import lombok.RequiredArgsConstructor;

import java.time.LocalDate;
import java.util.stream.Stream;

@RequiredArgsConstructor
public class ByDateTransactionHistoryFilter implements TransactionHistoryFilter {

    private final LocalDate localDate;

    @Override
    public Stream<Transaction> filter(Stream<Transaction> transactions) {
        return transactions.filter(t -> t.getDate()
                                         .toLocalDate()
                                         .equals(localDate));
    }
}
