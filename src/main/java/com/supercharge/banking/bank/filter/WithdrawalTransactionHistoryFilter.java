package com.supercharge.banking.bank.filter;

import com.supercharge.banking.dto.Transaction;

import java.util.stream.Stream;

public class WithdrawalTransactionHistoryFilter implements TransactionHistoryFilter {

    @Override
    public Stream<Transaction> filter(Stream<Transaction> transactions) {
        return transactions.filter(t -> t.getAmount() < 0);
    }
}
