package com.supercharge.banking.user.account;

import com.supercharge.banking.dto.Transaction;
import com.supercharge.banking.dto.TransactionEvent;
import com.supercharge.banking.dto.converter.TransactionConverter;
import com.supercharge.banking.user.account.factory.InitialTransactionListFactory;
import lombok.EqualsAndHashCode;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

import static org.springframework.beans.factory.config.BeanDefinition.SCOPE_PROTOTYPE;


@Component()
@Scope(value = SCOPE_PROTOTYPE)
@EqualsAndHashCode
public class UserAccount {

    private static final String INVALID_BALANCE = "Invalid transaction, account balance can not be smaller than 0";

    private final TransactionConverter converter;

    private final LinkedList<Transaction> transactions;

    public UserAccount(TransactionConverter converter, InitialTransactionListFactory factory) {
        this.converter = converter;
        this.transactions = factory.create();
    }

    public void performTransaction(TransactionEvent event) {
        int newBalance = getNewBalance(event);
        if (newBalance < 0) {
            throw new IllegalStateException(INVALID_BALANCE);
        }

        Transaction transaction = converter.convert(event, newBalance);
        transactions.add(transaction);
    }

    public List<Transaction> getTransactionsView() {
        Integer currentBalance = getBalance();
        return transactions.stream()
                           .map(t -> Transaction.builder()
                                                .date(t.getDate())
                                                .amount(t.getAmount())
                                                .balance(currentBalance)
                                                .build())
                           .collect(Collectors.toList());
    }

    private int getNewBalance(TransactionEvent event) {
        return getBalance() + event.getAmount();
    }

    private Integer getBalance() {
        return transactions.getLast()
                           .getBalance();
    }
}

