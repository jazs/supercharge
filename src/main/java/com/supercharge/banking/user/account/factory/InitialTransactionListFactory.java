package com.supercharge.banking.user.account.factory;

import com.supercharge.banking.dto.Transaction;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.time.Clock;
import java.time.LocalDateTime;
import java.util.LinkedList;

@Component
@RequiredArgsConstructor
public class InitialTransactionListFactory {

    private final Clock clock;

    public LinkedList<Transaction> create() {
        LinkedList<Transaction> transactions = new LinkedList<>();
        Transaction initialTransaction = Transaction.builder()
                                                    .date(LocalDateTime.now(clock))
                                                    .amount(0)
                                                    .balance(0)
                                                    .build();

        transactions.add(initialTransaction);

        return transactions;
    }

}
