package com.supercharge.banking.user.account.factory;

import com.supercharge.banking.dto.converter.TransactionConverter;
import com.supercharge.banking.user.account.UserAccount;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class UserAccountFactory {

    private final TransactionConverter converter;

    private final InitialTransactionListFactory initialTransactionListFactory;

    public UserAccount create() {
        return new UserAccount(converter, initialTransactionListFactory);
    }



}
