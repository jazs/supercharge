package com.supercharge.banking.user;

import com.supercharge.banking.user.account.UserAccount;
import com.supercharge.banking.user.account.factory.UserAccountFactory;
import lombok.Getter;
import lombok.NonNull;

@Getter
public class User {

    @NonNull
    private final String name;

    @NonNull
    private final UserAccount userAccount;

    public User(String name, UserAccountFactory userAccountFactory) {
        this.name = name;
        this.userAccount = userAccountFactory.create();
    }
}
