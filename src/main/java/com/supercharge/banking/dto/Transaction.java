package com.supercharge.banking.dto;

import lombok.Builder;
import lombok.NonNull;
import lombok.Value;

import java.time.LocalDateTime;

@Value
@Builder
public class Transaction {

    @NonNull
    private LocalDateTime date;

    @NonNull
    private Integer amount;

    @NonNull
    private Integer balance;
}

