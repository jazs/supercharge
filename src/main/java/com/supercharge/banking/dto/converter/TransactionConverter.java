package com.supercharge.banking.dto.converter;

import com.supercharge.banking.dto.Transaction;
import com.supercharge.banking.dto.TransactionEvent;
import org.springframework.stereotype.Component;

@Component
public class TransactionConverter {

    public Transaction convert(TransactionEvent event, int newBalance) {
        return Transaction.builder()
                          .date(event.getDate())
                          .amount(event.getAmount())
                          .balance(newBalance)
                          .build();
    }

}
