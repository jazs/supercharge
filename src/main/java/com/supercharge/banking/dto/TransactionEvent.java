package com.supercharge.banking.dto;

import lombok.NonNull;
import lombok.Value;

import java.time.LocalDateTime;

@Value
public class TransactionEvent {

    @NonNull
    private LocalDateTime date;

    @NonNull
    private Integer amount;

}
