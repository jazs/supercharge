package com.supercharge.banking;

import com.supercharge.banking.bank.Bank;
import com.supercharge.banking.bank.filter.ByDateTransactionHistoryFilter;
import com.supercharge.banking.bank.filter.DepositTransactionHistoryFilter;
import com.supercharge.banking.bank.filter.TransactionHistoryFilter;
import com.supercharge.banking.user.User;
import com.supercharge.banking.user.account.factory.UserAccountFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

import java.time.LocalDateTime;
import java.util.List;

import static java.util.Arrays.asList;

@SpringBootApplication
public class BankingApplication implements CommandLineRunner {

    private static final String PALI = "Pali";
    private static final String PISTA = "Pista";
    private static final LocalDateTime NOW = LocalDateTime.now();
    @Autowired
    private ApplicationContext appContext;

    public static void main(String[] args) {
        SpringApplication.run(BankingApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {

        Bank bank = appContext.getBean(Bank.class);
        UserAccountFactory userAccountFactory = appContext.getBean(UserAccountFactory.class);

        User pali = new User(PALI, userAccountFactory);
        User pista = new User(PISTA, userAccountFactory);

        bank.addUser(pali);
        bank.addUser(pista);

        bank.deposit(NOW.plusDays(1), PALI, 100);
        bank.withdrawal(NOW.plusDays(1), PALI, 10);

        bank.deposit(NOW.plusDays(2), PALI, 100);
        bank.withdrawal(NOW.plusDays(2), PALI, 10);

        bank.deposit(NOW.plusDays(3), PALI, 100);
        bank.withdrawal(NOW.plusDays(3), PALI, 10);

        bank.transfer(NOW, PALI, PISTA, 10);

        ByDateTransactionHistoryFilter dateFilter = new ByDateTransactionHistoryFilter(NOW.toLocalDate()
                                                                                          .plusDays(3));
        DepositTransactionHistoryFilter depositFilter = new DepositTransactionHistoryFilter();
        List<TransactionHistoryFilter> filters = asList(dateFilter, depositFilter);

        //Printing Palis history by date and deposit
        bank.printHistory(PALI, filters);
    }

}
